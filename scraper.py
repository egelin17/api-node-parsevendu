from requests import get
from bs4 import BeautifulSoup
import time
import json

# Quand on fait le call api on doit envoyer en param : la ville et le code inse
url_pv = 'https://www.paruvendu.fr/animmos/listefo/default/default?fulltext=&idtag=&elargrayon=1&ray=50&lo=&codeINSEE=&cp=&pa=FR&chaine=L&ray=50&rfam=&typeService='
response = get(url_pv)
html_txt = BeautifulSoup(response.text, 'html.parser')
div_table = html_txt.find('div', attrs = {'class':'pv14listeannonces'}) 
num_service = 1
annonces = []
def return_value(value):
  print(value)

for row in div_table.findAll('div', attrs = {'class':'lazyload_bloc ergov3-annonce'}):
	if(num_service > 20):
		break
	else :
		link = row.find('a', attrs = {'class':'voirann'})
		time.sleep(10)
		response_service = get(link['href'])
		soup = BeautifulSoup(response_service.text, 'html.parser')
		
		if soup.find('form', attrs = {'id':'frmCaptcha'}):
			return_value('error')
			break
		else:
			div1 = soup.find('div', attrs = {'class':'im12_txt_ann im12_txt_ann_auto'}).get_text()
			div2 = soup.find('h1', attrs = {'id':'detail_h1'}).get_text()
			div3 = soup.find('span', attrs = {'id':'detail_loc'}).get_text()
			img = soup.find('img', attrs = {'class':'im11_pic_main'})
			if(img) : 
				img_src = img['src'] 
			else : 
				img_src = 'nothing'
			annonce = {"id": num_service, "url": link['href'], "description": div1, "title": div2, "loca": div3, "img": img_src}
			
			annonces.append(annonce)
			num_service = num_service + 1

jsonString = json.dumps(annonces)
jsonFile = open("data.json", "w")
jsonFile.write(jsonString)
jsonFile.close()
return_value('success')

