const PORT = process.env.PORT || 9000
const express = require('express')
//Récupération des données depuis le fichier Json
const services = require('./data.json');

const app = express()

//Page de menu
app.get('/', (req, res) => {
    res.json('Projet école : ESGI - IW5 : projet à but scolaire uniquement.')
})

//Retourne tout les services présent dans le fichier
app.get('/api/v1/resources/services/all', (req, res) => {
    if (service.length == 0) {
        res.json('Aucun résultat.')
    } else {
        res.json(services)
    }
})

//Retourne le service présent dans le fichier selon ID
app.get('/api/v1/resources/service/id/:serviceId', (req, res) => {
    const serviceId = req.params.serviceId
    if (isNaN(serviceId)) {
        res.json('ID non valide.')
    }
    var service = services.filter(function (item) {
        return item.id == serviceId;
    });
    if (service.length == 0) {
        res.json('Aucun résultat.')
    } else {
        res.json(service)
    }

})

//Retourne les services présent dans le fichier selon Localisation
app.get('/api/v1/resources/service/localisation/:localisation', (req, res) => {
    const localisation = req.params.localisation
    if (isNaN(localisation)) {
        res.json('Localisation non valide.')
    }
    const servicesAll = []
    for (var i = 0; i < services.length; i++) {
        text = services[i].loca
        if (text.includes(localisation)) {
            servicesAll.push(services[i])
        }
    }
    if (servicesAll.length == 0) {
        res.json('Aucun résultat.')
    } else {
        res.json(servicesAll);
    }

})

//Lance le script python
app.get('/api/v1/resources/service/export', (req, res) => {
    const { spawn } = require('child_process');

    return new Promise((resolve, reject) => {
        const childPython = spawn('python', ['./scraper.py']);
        childPython.stdout.on(`data`, (data) => {
            str = data.toString()
            console.log(data.toString())
            if (str.includes("error")) {
                res.json('Erreur durant l\'import. ERREUR : CAPTCHA.')
            } else {
                res.json('Import réussi avec succès.')
            }
        });
    })
})

//Listener du port pour le serveur
app.listen(PORT, () => console.log(`server running on PORT ${PORT}`))
